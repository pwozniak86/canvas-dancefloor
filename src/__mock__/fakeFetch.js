import sampleJson from "./sampleJson";

const fakeFetch = () => Promise.resolve({ json: () => Promise.resolve(sampleJson) });

export default fakeFetch;
