import styled from 'styled-components'
import { css } from "@styled-system/css"

export default styled('div')(css({
    maxWidth:'1200px',
    margin:'auto'
}))