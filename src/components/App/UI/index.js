import React, {useState} from "react";
import { connect } from "react-redux";
import * as dancefloorActions from "../../../store/dancefloor/actions";
import Container from './styled'

function UI({ data, dispatch }) {

	const [grid, setGrid] = useState({rows:2, columns:4})

	/*
    const onRandomizeClick = (event) => {
		event.preventDefault()
		dispatch(dancefloorActions.generateRandomGrid());
	};
	*/

	const onGenerateClick = (event) => {
		event.preventDefault()

		isGridSizeMatching() ? dispatch(dancefloorActions.generateRandomGrid()) : dispatch(dancefloorActions.setGridSize(grid));
	};

    const onColumnsChange = (event) => {
		const columns = parseInt(event.target.value)
		if(!isNaN(columns))
			setGrid({...grid, columns})
    }

    const onRowsChange = (event) => {
		const rows = parseInt(event.target.value)
		if(!isNaN(rows))
			setGrid({...grid, rows})
    }

	const isGridSizeMatching = () => {
		return data.columns === grid.columns && data.rows === grid.rows
	}

	return (
		<Container>
			<input type='number' id="columns" name="columns" min="1" max="99" placeholder = 'Number of columns' onChange={onColumnsChange}/>
			<input type='number' id="rows" name="rows" min="1" max="99"  placeholder = 'Number of rows' onChange={onRowsChange}/>
			<div onClick={onGenerateClick}>{isGridSizeMatching() ? 'Randomize' : 'Generate'}</div>
		</Container>
	);
}

export default connect((store) => {
	return { data: store.dancefloor };
})(UI);
