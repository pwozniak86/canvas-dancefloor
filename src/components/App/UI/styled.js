import styled from 'styled-components'
import { css } from "@styled-system/css"

export default styled('div')(css({
    display:'flex',
    flexDirection: ['column', 'row'],
    gap:'8px',
    margin:'20px 0',
    padding:['6px', 0],
    'input, button, div':{
        height:'80px',
        padding: 0, 
        border: '1px solid #666',
        minWidth: '120px',
        width:'100%', 
        textAlign:'center',
    }, 
    'div':{
        fontSize:'12px',
        display:'flex',
        flexDirection:'column',
        justifyContent:'center', 
        cursor:'pointer',
        userSelect: 'none',
        width:['100%', '80%'],
        ":hover":{
            backgroundColor:'#666',
            color:'#fff',
            
        }
        
    }
}))