import React, { useEffect } from "react";
import { connect } from "react-redux";
import Dancefloor from "./Dancefloor";
import UI from "./UI";
import * as appActions from "../../store/app/actions";
import Container from './styled'

function App({ app, dispatch }) {
	// component did mount
	useEffect(() => {
		dispatch(appActions.fetchDataFromApi);
	}, []);

	return (
		<Container>
			<UI />
			<Dancefloor />
		</Container>
	);
}

export default connect((store) => {
	return { app: store.app };
})(App);
