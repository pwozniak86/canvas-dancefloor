import styled from 'styled-components'
import { css } from "@styled-system/css"

export default styled('canvas')(css({
    cursor:'pointer',
    width:'100%'
}))