import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from '../../../store/dancefloor/actions'
import getRandomColor from '../../../utils/colorUtils'
import Canvas from './styled'

function Dancefloor({ data, dispatch }) {
	const canvasRef = useRef(null);

	// component did mount
	useEffect(() => {
		draw();
	}, []);

	// data updated
	useEffect(() => {
		draw();
	}, [data]);

	const resolveProperties = () => {
		const { rows, columns, colors } = data;
		const context = canvasRef.current.getContext("2d");
		const fieldWidth = Math.ceil(context.canvas.width / columns);
		const fieldHeight = Math.ceil(context.canvas.height / rows);
		
		return {rows, columns, colors, context, fieldWidth, fieldHeight}
	}

	const draw = () => {
		const {rows, columns, colors, context, fieldWidth, fieldHeight} = resolveProperties()

		for (let x = 0; x < rows; x++) {
			for (let y = 0; y < columns; y++) {
				const xOffset = y * fieldWidth;
				const yOffset = x * fieldHeight;
				const fieldIndex = x * columns + y;

				context.fillStyle = colors[fieldIndex];
				context.fillRect(xOffset, yOffset, fieldWidth, fieldHeight);
			}
		}
	};

	const onClick = (event) => {
        const {columns, fieldWidth, fieldHeight} = resolveProperties()
        
        const rect = canvasRef.current.getBoundingClientRect()
        const cursorX = event.clientX - rect.left
        const cursorY = event.clientY - rect.top

        const fieldX = Math.floor(cursorX / fieldWidth)
        const fieldY = Math.floor(cursorY / fieldHeight)

        const fieldIndex = fieldY * columns + fieldX;

        dispatch(actions.updateField({index:fieldIndex, color:getRandomColor()}))
    };

	return <Canvas ref={canvasRef} width="1200px" height="600px" onClick={onClick} />;
}

export default connect((store) => {
	return { data: store.dancefloor };
})(Dancefloor);
