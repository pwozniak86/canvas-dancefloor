import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { compose, combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import "./index.css";
import App from "./components/App";
import appReducer from "./store/app/reducer";
import dancefloorReducer from "./store/dancefloor/reducer";

const store = createStore(combineReducers({ app: appReducer, dancefloor: dancefloorReducer }), compose(applyMiddleware(thunk)));

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<App />
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
);
