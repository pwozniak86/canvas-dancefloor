import * as labels from './labels'

export const updateField = (data) => {
    return({type:labels.UPDATE_DANCEFLOOR_FIELD, data})
}

export const generateRandomGrid = () => {
    return({type:labels.GENERATE_RANDOM_GRID})
}

export const setGridSize = (data) => {
    return({type:labels.SET_GRID_SIZE, data})
}
