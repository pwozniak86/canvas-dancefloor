export const SET_DANCEFLOOR_DATA = "setDancefloorData";
export const UPDATE_DANCEFLOOR_FIELD = "updateDancefloorField";
export const GENERATE_RANDOM_GRID = "generateRandomGrid";
export const SET_GRID_SIZE = "setGridSize";

