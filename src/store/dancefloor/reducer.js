import * as labels from "./labels";
import getRandomColor from '../../utils/colorUtils'

function fillColors(state){
	const {rows, columns, colors} = state;
	const fieldsNum = rows* columns;
	for (let i=0; i< fieldsNum; i++){
		colors[i] = (i < colors.length) ? colors[i] : getRandomColor();
	}
}

const initialState = {};

const reducer = (state = initialState, action) => {
	let newState;
	switch (action.type) {
		case labels.SET_DANCEFLOOR_DATA:
			return action.data;
		
		case labels.UPDATE_DANCEFLOOR_FIELD:
			const { index, color } = action.data;
			newState = {...state}
			newState.colors[index] = color;
			return newState;

		case labels.GENERATE_RANDOM_GRID:
			newState = {...state}
			newState.colors = newState.colors.map(item => getRandomColor())
			return newState;

		case labels.SET_GRID_SIZE:
			newState = {...state}
			newState.columns = action.data.columns;
			newState.rows = action.data.rows;
			fillColors(newState)
			return newState;

		default:
			return state;
	}
};

export default reducer;
