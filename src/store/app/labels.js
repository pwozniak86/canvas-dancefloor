export const FETCH_DATA_FROM_API = "fetchDataFromApi";
export const FETCH_DATA_FROM_API_COMPLETE = "fetchDataFromApiComplete";

export const SHOW_MODAL = "showModal";
export const HIDE_MODAL = "hideModal";