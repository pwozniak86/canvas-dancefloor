import fakeFetch from "../../__mock__/fakeFetch";
import * as appActionLabels from "./labels";
import * as dancefloorActionLabels from "../dancefloor/labels";

export const fetchDataFromApi = async (dispatch) => {
	const response = await fakeFetch();
	const json = await response.json();

	dispatch({ type: appActionLabels.FETCH_DATA_FROM_API_COMPLETE, data: {isLoading:false, error:null} });
	dispatch({ type: dancefloorActionLabels.SET_DANCEFLOOR_DATA, data: json });
};

export const showModal = (data) => {
    return({type:appActionLabels.SHOW_MODAL, data})
}

export const hideModal = () => {
    return({type:appActionLabels.SHOW_MODAL})
}