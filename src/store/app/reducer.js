import * as labels from './labels'


const initialState = {isLoading:true, error:null, modal:null}

const reducer = (state = initialState, action) => {
    let newState;
    switch (action.type) {
        case labels.FETCH_DATA_FROM_API_COMPLETE:
            return action.data;
        case labels.SHOW_MODAL:
            newState = {...state}
            newState.modal = action.data
            return newState;
        case labels.HIDE_MODAL:
            newState = {...state}
            newState.modal = null
            return newState;
        default:
            return state;
    }
}


export default reducer

